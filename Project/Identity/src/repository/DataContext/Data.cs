using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using Identity.Abstraction;

namespace repository
{
    public class Data : IData
    {
        private IMongoDatabase db;
        IMongoClient client = new MongoClient("mongodb://localhost/27017");
        public Data()
        {
            db = client.GetDatabase("OnlineExamRepo");
        }
        public IMongoCollection<UserDetails> UserData => db.GetCollection<UserDetails>("UserData");
        public IMongoCollection<LoginRequest> LoginData => db.GetCollection<LoginRequest>("LoginData");
        public IMongoCollection<UserDetails> ExamineeData => db.GetCollection<UserDetails>("ExamineeData");
        public IMongoCollection<CentreDetails> CentreData => db.GetCollection<CentreDetails>("CentreData");
        public IMongoCollection<Question> QuestionData => db.GetCollection<Question>("QuestionData");
    }

}