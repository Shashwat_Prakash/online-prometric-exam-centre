using MongoDB.Driver;
using Identity.Abstraction;
namespace repository
{
    public interface IData
    {
        IMongoCollection<UserDetails> UserData { get; }
        IMongoCollection<LoginRequest> LoginData { get; }
        IMongoCollection<UserDetails> ExamineeData { get; }
        IMongoCollection<CentreDetails> CentreData { get; }
        IMongoCollection<Question> QuestionData { get; }
    }
}