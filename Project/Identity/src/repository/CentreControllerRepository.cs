using System;
using System.Collections.Generic;
using Identity.Abstraction;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Bson.Serialization.Attributes;
//using MongoDB.Driver.Core;
//using Identity.Service;

namespace repository
{
    public class CentreControllerRepository :ICentreControllerRepository
    {
        private IData _data;
        public CentreControllerRepository(IData data)
        {
            _data = data;
        }
        
        public async Task AddCentre(CentreDetails centredetails)
        {
            await _data.CentreData.InsertOneAsync(centredetails);
        }

        public async Task<IEnumerable<CentreDetails>> AllCentres()
        {
            return await _data
                            .CentreData
                            .Find(_ => true)
                            .ToListAsync();
        } 

        public Task<CentreDetails> GetByIdCentre(int id)
        {
            FilterDefinition<CentreDetails> filter = Builders<CentreDetails>.Filter.Eq(m => m.Id, id);

            return _data
                    .CentreData
                    .Find(filter)
                    .FirstOrDefaultAsync();
            
        }

        public async Task<bool> UpdateCentre(CentreDetails centreDetails)
        {
            ReplaceOneResult updateResult =
                await _data
                        .CentreData
                        .ReplaceOneAsync(
                            filter: g => g.Id == centreDetails.Id,
                            replacement: centreDetails);

             return updateResult.IsAcknowledged
                     && updateResult.ModifiedCount > 0;
            
        }

        public async Task<bool> DeleteCentre(int id)
        {
            FilterDefinition<CentreDetails> filter = Builders<CentreDetails>.Filter.Eq(m => m.Id, id);

            DeleteResult deleteResult = await _data
                                                .CentreData
                                                .DeleteOneAsync(filter);

            return deleteResult.IsAcknowledged
                && deleteResult.DeletedCount > 0;
            
        }
    }
}
