using System;
using System.Collections.Generic;
using Identity.Abstraction;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Bson.Serialization.Attributes;

namespace repository
{
    public class ResetPasswordControllerRepository : IResetPasswordControllerRepository
    {
        IData _data;
        public ResetPasswordControllerRepository(IData data)
        {
            _data = data;
        }

        public async Task<bool> ChangePassword(int id, string password)
        {
            //UserDetails user = new UserDetails();
            //FilterDefinition<UserDetails> filter = Builders<UserDetails>.Filter.Eq(m => m.Id, id);
            //FilterDefinition<UserDetails> replace = Builders<UserDetails>.Filter.Eq(m => m.Password, resetRequest.NewPassword);
            //filter.Password = resetRequest.NewPassword;
            //UserDetails d=(UserDetails)filter;
            //Console.WriteLine("----------------------"+filter+"++++++++++++++++++++++++++");
            //var entry = new UserDetails { Password = resetRequest.NewPassword };
            //var options = new UpdateOptions { IsUpsert = true };
            ReplaceOneResult updateResult =
                await _data
                        .UserData
                        .ReplaceOneAsync(
                            Builders<UserDetails>.Filter.Eq(m => m.Id, id),
                            Builders<UserDetails>.Filter.Eq(_id => _id.Password, resetRequest.NewPassword),
                            new UpdateOptions { IsUpsert = true }
                        );

             return updateResult.IsAcknowledged
                     && updateResult.ModifiedCount > 0;
        }
    }
}