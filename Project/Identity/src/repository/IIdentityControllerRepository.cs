using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Identity.Abstraction;

namespace repository
{
    public interface IIdentityControllerRepository
    {
        Task AddUser(UserDetails userDetails);
        Task<IEnumerable<UserDetails>> AllDetails();
        Task<UserDetails> GetUserById(int id);
        Task<bool> Update(UserDetails userDetails);
        Task<bool> Delete(int id);
        Task Login(LoginRequest request);
    }
}