﻿using System;
using System.Collections.Generic;
using Identity.Abstraction;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Bson.Serialization.Attributes;
//using MongoDB.Driver.Core;
//using Identity.Service;

namespace repository
{
    public class IdentityControllerRepository : IIdentityControllerRepository
    {
        private IData _data;
        public IdentityControllerRepository(IData data)
        {
            _data = data;
        }

        public async Task AddUser(UserDetails userDetails)
        {
            await _data.UserData.InsertOneAsync(userDetails);
        }

        public async Task<IEnumerable<UserDetails>> AllDetails()
        {
            return await _data
                            .UserData
                            .Find(_ => true)
                            .ToListAsync();
        }

        public Task<UserDetails> GetUserById(int id)
        {
            FilterDefinition<UserDetails> filter = Builders<UserDetails>.Filter.Eq(m => m.Id, id);

            return _data
                    .UserData
                    .Find(filter)
                    .FirstOrDefaultAsync();
        }

        public async Task<bool> Update(UserDetails userDetails)
        {
            ReplaceOneResult updateResult =
                await _data
                        .UserData
                        .ReplaceOneAsync(
                            filter: g => g.Id == userDetails.Id,
                            replacement: userDetails);

            return updateResult.IsAcknowledged
                    && updateResult.ModifiedCount > 0;
        }

        public async Task<bool> Delete(int id)
        {
            FilterDefinition<UserDetails> filter = Builders<UserDetails>.Filter.Eq(m => m.Id, id);

            DeleteResult deleteResult = await _data
                                                .UserData
                                                .DeleteOneAsync(filter);

            return deleteResult.IsAcknowledged
                && deleteResult.DeletedCount > 0;
        }

        public async Task Login(LoginRequest request)
        {
            FilterDefinition<UserDetails> user = Builders<UserDetails>.Filter.Eq(m => m.Username, request.Username);
            FilterDefinition<UserDetails> pass = Builders<UserDetails>.Filter.Eq(m => m.Password, request.Password);
            await _data.LoginData.InsertOneAsync(request);

        }
    }
}
