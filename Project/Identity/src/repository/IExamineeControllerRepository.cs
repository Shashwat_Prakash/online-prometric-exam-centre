using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Identity.Abstraction;


namespace repository
{
    public interface IExamineeControllerRepository
    {
        //Task AddUser(UserDetails userdetails);
        Task<IEnumerable<UserDetails>> GetAllExaminees();
        Task<UserDetails> GetByIdExaminee(int id);
    }
}