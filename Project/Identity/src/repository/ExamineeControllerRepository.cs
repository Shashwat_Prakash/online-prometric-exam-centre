using System;
using System.Collections.Generic;
using Identity.Abstraction;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Bson.Serialization.Attributes;
//using MongoDB.Driver.Core;
//using Identity.Service;

namespace repository
{
    public class ExamineeControllerRepository :IExamineeControllerRepository
    {
        private IData _data;
        public ExamineeControllerRepository(IData data)
        {
            _data = data;
        }
        
        /*public async Task AddUser(UserDetails userDetails)
        {
            await _data.UserData.InsertOneAsync(userDetails);
        }
        */

        public async Task<IEnumerable<UserDetails>> GetAllExaminees()
        {
            return await _data
                            .UserData
                            .Find(_ => true)
                            .ToListAsync();
        } 
        public  Task<UserDetails> GetByIdExaminee(int id)
        {
            FilterDefinition<UserDetails> filter = Builders<UserDetails>.Filter.Eq(m => m.Id, id);

            return _data
                    .UserData
                    .Find(filter)
                    .FirstOrDefaultAsync();
        }
    }
}
