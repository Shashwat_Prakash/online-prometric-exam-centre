using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Identity.Abstraction;

namespace repository
{
    public interface IQuestionControllerRepository
    {
        Task AddQuestion(Question question);
        Task<bool> UpdateQuestion(Question question);
        Task<IEnumerable<Question>> AllQuestion();
        Task<Question> GetQuestionById(int id);
        Task<bool> DeleteQuestion(int id);
    }
}