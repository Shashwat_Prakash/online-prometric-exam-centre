using System;
using System.Collections.Generic;
using Identity.Abstraction;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Bson.Serialization.Attributes;

namespace repository
{
    public class QuestionControllerRepository : IQuestionControllerRepository
    {
        private IData _data;
        public QuestionControllerRepository(IData data)
        {
            _data = data;
        }

        public async Task AddQuestion(Question question)
        {
            await _data.QuestionData.InsertOneAsync(question);
        }

        public async Task<IEnumerable<Question>> AllQuestion()
        {
            return await _data
                            .QuestionData
                            .Find(_ => true)
                            .ToListAsync();
        }

        public Task<Question> GetQuestionById(int id)
        {
            FilterDefinition<Question> filter = Builders<Question>.Filter.Eq(m => m.Id, id);

            return _data
                    .QuestionData
                    .Find(filter)
                    .FirstOrDefaultAsync();
        }

        public async Task<bool> UpdateQuestion(Question question)
        {
            ReplaceOneResult updateResult =
                await _data
                        .QuestionData
                        .ReplaceOneAsync(
                            filter: g => g.Id == question.Id,
                            replacement: question);

            return updateResult.IsAcknowledged
                    && updateResult.ModifiedCount > 0;
        }

        public async Task<bool> DeleteQuestion(int id)
        {
            FilterDefinition<Question> filter = Builders<Question>.Filter.Eq(m => m.Id, id);

            DeleteResult deleteResult = await _data
                                                .QuestionData
                                                .DeleteOneAsync(filter);

            return deleteResult.IsAcknowledged
                && deleteResult.DeletedCount > 0;
        }
    }
}