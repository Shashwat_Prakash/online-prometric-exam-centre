using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Identity.Abstraction;

namespace repository
{
    public interface IResetPasswordControllerRepository
    {
        Task<bool> ChangePassword(int id, ResetPasswordRequest resetRequest);
    }
}