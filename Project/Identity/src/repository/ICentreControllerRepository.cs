using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Identity.Abstraction;

namespace repository
{
    public interface ICentreControllerRepository
    {
        Task AddCentre(CentreDetails centredetails);
        Task<IEnumerable<CentreDetails>> AllCentres();
        //Task AddUser(UserDetails userDetails);
        //Task<IEnumerable<UserDetails>> AllDetails();
        Task<CentreDetails> GetByIdCentre(int id);
        Task<bool> UpdateCentre(CentreDetails centreDetails);
        Task<bool> DeleteCentre(int id);
        //Task Login(LoginRequest request);
    }
}