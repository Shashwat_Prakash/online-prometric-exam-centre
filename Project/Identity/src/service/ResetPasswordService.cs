using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Identity.Abstraction;
using Identity.Service;
using repository;

namespace Identity.Service
{
    public class ResetPasswordService : IResetPasswordService
    {
        UserDetails userDetails = new UserDetails();
        IResetPasswordControllerRepository _passwordRepo;

        public ResetPasswordService(IResetPasswordControllerRepository passwordRepo)
        {
            _passwordRepo = passwordRepo;
        }
        public async Task<bool> ChangePassword(int Id, ResetPasswordRequest resetRequest)
        {
            // UserDetails user = IdentityService.details.Find(x => x.Id == Id);
            // if(user!=null)
            // {
            //     if(resetRequest.NewPassword != resetRequest.ConfirmPassword)
            //     {
            //         return null;
            //     }
            //     else
            //     {
            //         user.Password = resetRequest.NewPassword;
            //         return user;
            //     }
            // }
            // else
            // {
            //     return null;
            // }
            return await _passwordRepo.ChangePassword(Id, resetRequest);
        }
    }
}