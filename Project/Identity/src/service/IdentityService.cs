﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using System.Linq;
using Identity.Abstraction;
using repository;

namespace Identity.Service
{
    public class IdentityService : IIdentityService
    {
        public static List<UserDetails> details = new List<UserDetails>(); //Storing of User details in List Collection
        public static List<UserDetails> logReq = new List<UserDetails>();
        UserDetails user;
        IIdentityControllerRepository _repo;
        public IdentityService()
        {

        }
        public IdentityService(IIdentityControllerRepository repo)
        {
            _repo = repo;
        }

        public void Login(LoginRequest request)
        {
            // user = details.Find(uName => uName.Username==username);
            // UserDetails pass = details.Find(psword => psword.Password==password);
            // if(user != null && pass !=null)
            // {
            //     if(user == pass)
            //     {
            //         logReq.Add(user);
            //         return user;
            //     }
            //     else
            //     {
            //         return null;
            //     }
            // }
            // else
            // {
            //     return null;
            // }
            _repo.Login(request);

        }

        public void Logout()
        {
            logReq.Remove(user);
        }

        public UserDetails AddUser(UserDetails user)
        {
            details.Add(user);
            _repo.AddUser(user);
            return user;
        }

        public string AdminLogin(string username, string password)
        {
            string name1 = "admin1";
            string password1 = "root1";

            string name2 = "admin2";
            string password2 = "root2";

            string name3 = "admin3";
            string password3 = "root3";

            if ((username == name1 && password == password1) ||
               (username == name2 && password == password2) ||
               (username == name3 && password == password3))
                return username;
            else
                return null;
        }

        public Task<IEnumerable<UserDetails>> GetAll()
        {
            return _repo.AllDetails();
            // return details;
        }

        public Task<UserDetails> GetById(int id)
        {
            //UserDetails user = details.Find(x => x.Id == id);
            return _repo.GetUserById(id);
        }

        public async Task<bool> Delete(int id)
        {
            return await _repo.Delete(id);
        }

        public async Task<bool> UpdateData(UserDetails userDetails)
        {
            //UserDetails user = details.Find(x => x.Id==id);
            // if(user!=null)
            // {
            //     details.Remove(user);
            //     details.Add(userDetails);
            // }
            return await _repo.Update(userDetails);
        }
    }
}
