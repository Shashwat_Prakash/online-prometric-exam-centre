using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Identity.Abstraction;
using repository;
namespace Identity.Service
{
    public class QuestionService : IQuestionService
    {
        static List<Question> details = new List<Question>(); //Storing the Ques details in List Collection
        IQuestionControllerRepository _repo;
        public QuestionService(IQuestionControllerRepository repo)
        {
            _repo = repo;
        }
        public Question AddQues(Question question)
        {
            details.Add(question);
            _repo.AddQuestion(question);
            return question;
        }

        public Task<IEnumerable<Question>> GetAll()
        {
            return _repo.AllQuestion();
            // return details;
        }

        public Task<Question> GetById(int id)
        {
            //UserDetails user = details.Find(x => x.Id == id);
            return _repo.GetQuestionById(id);
        }
        public async Task<bool> UpdateData(Question question)
        {
            return await _repo.UpdateQuestion(question);
        }

        public async Task<bool> Delete(int id)
        {
            return await _repo.DeleteQuestion(id);
        }
    }
}