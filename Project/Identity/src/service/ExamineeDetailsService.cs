using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Identity.Abstraction;
using repository;


namespace Identity.Service
{
    public class ExamineeDetailsService : IExamineeDetailsService
    {
        public static List<UserDetails> examinees = new List<UserDetails>(); //Storing of User details in List Collection
        UserDetails examinee;
        IExamineeControllerRepository _repo;

         public ExamineeDetailsService(IExamineeControllerRepository repo)
        {
            _repo = repo;
        }
        public Task<IEnumerable<UserDetails>> GetAllExaminees()
        {
           return _repo.GetAllExaminees();
           
        }

        public Task<UserDetails> GetByIdExaminee(int id)
        {
            //UserDetails examinee = examinees.Find(x => x.Id == id);
            return _repo.GetByIdExaminee(id);
        }
    }
}
