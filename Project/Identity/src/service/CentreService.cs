using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using MongoDB.Bson;
using System.Linq;
using Identity.Abstraction;
using repository;

namespace Identity.Service
{
    public class CentreService : ICentreService
    {
        public static List<CentreDetails> centredetails = new List<CentreDetails>(); //Storing of User details in List Collection

        CentreDetails centre;
        ICentreControllerRepository _repo;

        public CentreService(ICentreControllerRepository repo)
        {
            _repo = repo;
        }

        public CentreDetails AddCentre(CentreDetails centre)
        {
            centredetails.Add(centre);
            _repo.AddCentre(centre);
            return centre;
        }



        public Task<IEnumerable<CentreDetails>> GetAllCentres()
        {
            return _repo.AllCentres();
        }

        public Task<CentreDetails> GetByIdCentre(int id)
        {
            return _repo.GetByIdCentre(id);
        }

        public async Task<bool> DeleteCentre(int id)
        {
            return await _repo.DeleteCentre(id);
        }

        public async Task<bool> UpdateCentre(CentreDetails centreDetails)
        {
            return await _repo.UpdateCentre(centreDetails);
        }
    }
}
