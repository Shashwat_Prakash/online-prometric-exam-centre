using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Identity.Abstraction;

namespace Identity.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ResetPasswordController : Controller
    {
        private IResetPasswordService _resetService;
        public ResetPasswordController(IResetPasswordService resetService)
        {
            _resetService = resetService;
        }

        [Route("{id}")]
        [HttpPut]
        public async Task<IActionResult> Reset(int id, [FromBody]ResetPasswordRequest resetRequest)
        {
            // UserDetails user = _resetService.ChangePassword(Id ,resetRequest);
            // return Ok(new {message = "Password change"});
             _resetService.ChangePassword(id, resetRequest);
            return new OkResult();
        }
    }
}