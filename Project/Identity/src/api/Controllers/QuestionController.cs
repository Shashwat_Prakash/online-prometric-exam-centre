using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Identity.Abstraction;

namespace Identity.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class QuestionController : ControllerBase
    {
        private IQuestionService _service;
        public QuestionController(IQuestionService questionservice)       //Dependency Injection for IQuesServices
        {
            _service = questionservice;
        }

        [Route("addquestion")]                                         //Add question Api
        [HttpPost]
        public IActionResult AddQues([FromBody]Question question)
        {
            Question quess = _service.AddQues(question);
            if (quess != null)
            {
                return Ok(new { message = "Question added successfully" });
            }
            return BadRequest();
        }

        [Route("viewquestion")]                                  //Fetching All details Api
        [HttpGet]
        public Task<IEnumerable<Question>> GetAllDetails()
        {
            Task<IEnumerable<Question>> questions = _service.GetAll();
            return questions;
        }

        [Route("{id}")]                                             //Fetching details based on Id Api
        [HttpGet]
        public Task<Question> GetByQuesId(int id)
        {
            Task<Question> quesById = _service.GetById(id);
            if (quesById != null)
            {
                return quesById;
            }
            return null;
        }

        [Route("{id}")]
        [HttpPut]
        public async Task<IActionResult> Update(int id, [FromBody]Question question)
        {
            var ques = await _service.GetById(id);
            if (ques == null)
                return new NotFoundResult();
            question.Id = ques.Id;

            await _service.UpdateData(question);
            return new OkResult();
        }

        [Route("{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var question = await _service.GetById(id);
            if (question == null)
                return new NotFoundResult();
            await _service.Delete(id);
            return new OkResult();
        }
    }
}
