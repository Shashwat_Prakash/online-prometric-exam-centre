﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Identity.Abstraction;
using repository;

namespace Identity.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class IdentityController : ControllerBase
    {
        private IIdentityService _service;
        public IdentityController(IIdentityService idservice)       //Dependency Injection for IIdentityServices
        {
            _service = idservice;
            
        }

        // POST api/values
        [Route("userlogin")]                                        //User Login Api
        [HttpPost]
        public void Post([FromBody] LoginRequest request)    
        {
            _service.Login(request);
        }
        
        [Route("logout")]                                           //Logout Api
        [HttpPost]
        public IActionResult Post()
        {
            _service.Logout();
            return Ok(new {message ="Succesfully logged out"});
        }
        
        [Route("register")]                                         //User Regitration Api
        [HttpPost]
        public IActionResult Register([FromBody]UserDetails userDetails)
        {
           // _repo.AddUser(userDetails);
            UserDetails user = _service.AddUser(userDetails);
            if(user != null)
            {
                return Ok(new {message = userDetails.Firstname +" ,You are registered"});
            }
            return BadRequest();
        }

        [Route("adminlogin")]                                       //Adminstrator Login Api
        [HttpPost]
        public IActionResult Admin([FromBody]LoginRequest request)
        {
            var admin = _service.AdminLogin(request.Username, request.Password);
            if(admin != null)
                return Ok(new {message = request.Username+" , Welcome"});
            else
                return BadRequest(new {message = request.Username+" , Not Found"});
        }

        [Route("alldetails")]                                       //Fetching All details Api
        [HttpGet]
        public Task<IEnumerable<UserDetails>> GetAllDetails()
        {
           Task<IEnumerable<UserDetails>> users =  _service.GetAll();
           return users;
        }

        [Route("{id}")]                                             //Fetching details based on Id Api
        [HttpGet]
        public Task<UserDetails> GetById(int id)
        {
           Task<UserDetails> userById = _service.GetById(id);
           if(userById!=null)
           {
               return userById;
           }
           return null;
        }

        [Route("{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var  user = await _service.GetById(id);
            if (user == null)
                return new NotFoundResult();
            await _service.Delete(id);
                return new OkResult();
        }

        [Route("{id}")]
        [HttpPut]
        public async Task<IActionResult> Update(int id, [FromBody]UserDetails userDetails)
        {
            var  user = await _service.GetById(id);
            if (user == null)
                return new NotFoundResult();
            userDetails.Id = user.Id;

            await _service.UpdateData(userDetails);
            return new OkResult();
        }
    }
}

