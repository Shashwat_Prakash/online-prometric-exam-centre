using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Identity.Abstraction;

namespace Identity.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ExamineeDetailsController : ControllerBase
    {
        private IExamineeDetailsService _service;
        public ExamineeDetailsController(IExamineeDetailsService examineeservice)       //Dependency Injection for IIdentityServices
        {
            _service = examineeservice;
        }

        [Route("allexaminees")]                                       //Fetching All details Api
        [HttpGet]
        public Task<IEnumerable<UserDetails>> GetAllExaminees()
        {
           Task<IEnumerable<UserDetails>> examinees =  _service.GetAllExaminees();
           return examinees;
        }

        [Route("{id}")]                                             //Fetching details based on Id Api
        [HttpGet]
        public Task<UserDetails> GetByIdExaminee(int id)
        {
           Task<UserDetails> examineeById = _service.GetByIdExaminee(id);
           if(examineeById!=null)
           {
               return examineeById;
           }
           return null;
        }

    }
}

