using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Identity.Abstraction;

namespace Identity.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CentreController : ControllerBase
    {
        private ICentreService _service;
        public CentreController(ICentreService centreservice)       //Dependency Injection for Centre services
        {
            _service = centreservice;
        }

        [Route("addcentre")]                                         //Add Centre api
        [HttpPost]
        public IActionResult AddCentre([FromBody]CentreDetails centreDetails)
        {
            CentreDetails centre = _service.AddCentre(centreDetails);
            if(centre != null)
            {
                return Ok(new {message = centreDetails.CentreName+" ,Centre Added"});
            }
            return BadRequest();
        }

        

        [Route("allcentres")]                                       //Fetching All details Api
        [HttpGet]
        public Task<IEnumerable<CentreDetails>> GetAllCentres()
        {
           Task<IEnumerable<CentreDetails>> centres =  _service.GetAllCentres();
           return centres;
        }

        [Route("{id}")]                                             //Fetching details based on Id Api
        [HttpGet]
        public Task<CentreDetails> GetByIdCentre(int id)
        {
           Task<CentreDetails> centreById = _service.GetByIdCentre(id);
           if(centreById!=null)
           {
               return centreById;
           }
           return null;
        }

        [Route("{id}")]                                          //Deleting Centre api
        [HttpDelete]
         public async Task<IActionResult> DeleteCentre(int id)
        {
            var  centre = await _service.GetByIdCentre(id);
            if (centre == null)
                return new NotFoundResult();
            await _service.DeleteCentre(id);
                return new OkResult();
        }

        [Route("{id}")]                                         //Updating Centre api
        [HttpPut]
        public async Task<IActionResult> UpdateCentre(int id, [FromBody]CentreDetails centreDetails)
        {
           var  centre = await _service.GetByIdCentre(id);
            if (centre == null)
                return new NotFoundResult();
            centreDetails.Id = centre.Id;

            await _service.UpdateCentre(centreDetails);
            return new OkResult();
        }
    }
}

