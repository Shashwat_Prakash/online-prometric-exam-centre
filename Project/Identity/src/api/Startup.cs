﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Identity.Abstraction;
using Identity.Service;
using repository;

namespace Identity.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddScoped<IIdentityService, IdentityService>();
            services.AddScoped<IResetPasswordService, ResetPasswordService>();
            services.AddScoped<IExamineeDetailsService, ExamineeDetailsService>();
            services.AddScoped<ICentreService, CentreService>();
            services.AddScoped<IQuestionService, QuestionService>();
            services.AddScoped<IData, Data>();
            services.AddScoped<IIdentityControllerRepository, IdentityControllerRepository>();
            services.AddScoped<IExamineeControllerRepository, ExamineeControllerRepository>();
            services.AddScoped<ICentreControllerRepository, CentreControllerRepository>();
            services.AddScoped<IQuestionControllerRepository, QuestionControllerRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            //app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
