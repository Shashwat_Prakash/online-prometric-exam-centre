using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Identity.Abstraction
{
    public interface IResetPasswordService
    {
        Task<bool> ChangePassword(int Id, ResetPasswordRequest resetRequest);
    }
}