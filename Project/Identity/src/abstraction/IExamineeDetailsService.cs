using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Identity.Abstraction
{
    public interface IExamineeDetailsService
    {
        Task<IEnumerable<UserDetails>> GetAllExaminees();
        Task<UserDetails> GetByIdExaminee(int id);
        
    }
}