using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Identity.Abstraction
{
    public interface IQuestionService
    {
        Question AddQues(Question question);
        Task<IEnumerable<Question>> GetAll();
        Task<Question> GetById(int id);
        Task<bool> Delete(int id);
        Task<bool> UpdateData(Question question);
    }
}