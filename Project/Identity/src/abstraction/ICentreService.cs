using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Identity.Abstraction
{
    public interface ICentreService
    {
        //UserDetails Login(string userName, string password); 
        CentreDetails AddCentre(CentreDetails centre); 
        //string AdminLogin(string username, string paswword); 
        Task<IEnumerable<CentreDetails>> GetAllCentres();
        Task<CentreDetails> GetByIdCentre(int id);
        Task<bool> DeleteCentre(int id);
        Task<bool> UpdateCentre(CentreDetails centreDetails);
    }
}