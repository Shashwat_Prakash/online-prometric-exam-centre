namespace Identity.Abstraction
{   
    public interface ILoginRequest
    {
        string Username {get; set;}
        string Password {get; set;}
    }
}