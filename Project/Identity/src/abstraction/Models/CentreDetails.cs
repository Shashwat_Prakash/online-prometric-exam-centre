
namespace Identity.Abstraction
{   
    public class CentreDetails : ICentreDetails
    {
       public int Id {get ; set ;}
       public string CentreName { get ; set;}
       public string City { get ; set;}
       public string District { get ; set;}
       public string State { get ; set;}
       public int PinCode { get ; set;}
       public int ContactNo { get ; set;}
       public string EmailAddress { get ; set;}
    }
}
