namespace Identity.Abstraction
{
    public interface IResetPasswordRequest
    {
         int Id{get; set;}
         string Password{get; set;}
         string NewPassword{get; set;}
         string ConfirmPassword{get; set;}
    }
}