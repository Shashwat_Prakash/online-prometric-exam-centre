namespace Identity.Abstraction
{
    public interface IQuestion
    {
         int Id { get; set; }
         string Problem { get; set; }
         string Option1{get; set;}
         string Option2{get; set;}
         string Option3{get; set;}
         string Option4{get; set;}
         string Type { get; set; }
         string Marks { get; set; }
         string Answer { get; set; }
    }
}