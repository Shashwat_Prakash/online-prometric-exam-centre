namespace Identity.Abstraction
{
    public class Question : IQuestion
    {
        public int Id { get; set; }
        public string Problem { get; set; }
        public string Option1{get; set;}
        public string Option2{get; set;}
        public string Option3{get; set;}
        public string Option4{get; set;}
        public string Type { get; set; }
        public string Marks { get; set; }
        public string Answer { get; set; }
    }
}