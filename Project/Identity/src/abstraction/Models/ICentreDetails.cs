
namespace Identity.Abstraction
{   
    public interface ICentreDetails
    {
       int Id {get ; set ;}
       string CentreName { get ; set;}
       string City { get ; set;}
       string District { get ; set;}
       string State { get ; set;}
       int PinCode { get ; set;}
       int ContactNo { get ; set;}
       string EmailAddress { get ; set;}
    }
}