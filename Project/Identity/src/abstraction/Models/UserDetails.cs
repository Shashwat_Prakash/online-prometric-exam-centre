
namespace Identity.Abstraction
{
    public class UserDetails : IUserDetails
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ExamCenter { get; set; }
        public long Contact { get; set; }
    }
}
