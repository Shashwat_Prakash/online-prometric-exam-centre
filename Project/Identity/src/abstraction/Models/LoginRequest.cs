
namespace Identity.Abstraction
{   
    public class LoginRequest : ILoginRequest
    {
        public string Username {get; set;}
        public string Password {get; set;}
    }
}
