
namespace Identity.Abstraction
{   
    public interface IUserDetails
    {
         int Id {get; set;}
         string Firstname {get; set;}
         string Lastname {get; set;}
         string Username {get; set;}
         string Password {get; set;}
         string City {get; set;}
         string State {get; set;}
         string Country {get; set;}
         string ExamCenter {get; set;}
         long Contact {get; set;}
    }
}