﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Identity.Abstraction
{
    public interface IIdentityService
    {
        void Login(LoginRequest request); 
        UserDetails AddUser(UserDetails user); 
        string AdminLogin(string username, string paswword); 
        Task<IEnumerable<UserDetails>> GetAll();
        Task<UserDetails> GetById(int id);
        Task<bool> Delete(int id);
        Task<bool> UpdateData(UserDetails userDetails);
        void Logout(); 
    }
}
