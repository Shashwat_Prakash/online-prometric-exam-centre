﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Identity.Service;
using Identity.Abstraction;
namespace Test
{
    [TestFixture]
    public class IdentityControllerTest
    {
        private UserDetails Register()
        {
            UserDetails user = new UserDetails();
            user.Id = 903;
            user.Firstname = "Shashwat";
            user.Lastname = "Prakash";
            user.Username = "shashwat.prakash@gmail.com";
            user.Password = "1234";
            user.City = "Bangalore";
            user.State = "Karnataka";
            user.Country = "India";
            user.ExamCenter = "Pune";
            user.Contact = 9066740766;
            return user;
        }

        [Test]
        public void AddUserTest()
        {
            var user = Register();
            var _service = new IdentityService();               //Arrange

            UserDetails _result = _service.AddUser(user);       //Act

            Assert.That(_result, Is.EqualTo(user));             //Assertion
        }

        private List<UserDetails> AllUsers()
        {
            var users = new List<UserDetails>();
            users.Add(new UserDetails { Id = 1, Firstname = "Shashwat" });
            return users;
        }

        [Test]
        public void GetAllTest()
        {
            var users = AllUsers();
            var _service = new IdentityService();                   //Arrange

            var result = _service.GetAll() as List<UserDetails>;    //Act

            Assert.AreEqual(users.Count, result.Count);             //Assertion
        }

        [Test]
        public void GetByIdTest()
        {
            var users = Register();
            var _service = new IdentityService();                   //Arrange

            var result = _service.GetById(90);                       //Act

            Assert.AreEqual(users.Id, result.Id);                   //Assertion
        }

        [Test]
        public void DeleteTest()
        {
            var user = Register();
            var _service = new IdentityService();                   //Arrange

            _service.Delete(903);                               //Act

            Assert.IsNotNull(user);                             //Assertion
        }

        [Test]
        [TestCase("admin1", "root1", "admin1")]
        [TestCase("admin2", "root2", "admin2")]
        [TestCase("admin3", "root3", "admin3")]
        public void AdminLoginTest(string name, string password, string res)
        {
            var _service = new IdentityService();
            var result = _service.AdminLogin(name, password);
            Assert.AreEqual(res, result);
        }

        [Test]
        [TestCase("shashwat.prakash@gmail.com", "1234")]
        //[TestCase("rahul.prakash@gmail.com","4321")]
        public void UserLoginTest(string email, string pass)
        {
            var user = Register();
            var name = user.Username;
            var password = user.Password;

            var _service = new IdentityService();
            var userdata = _service.Login(email, pass);

            Assert.IsNotNull(userdata);
        }
    }
}
